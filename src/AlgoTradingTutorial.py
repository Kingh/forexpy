import time
import numpy as np
import matplotlib
import functools
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.dates as mdates

totalStart = time.time()


def convert_date(date_bytes):
    return mdates.strpdate2num('%Y%m%d%H%M%S')(date_bytes.decode('ascii'))


date, bid, ask = np.loadtxt(
    '../GBPUSD1d.txt',
    unpack=True,
    delimiter=',',
    converters={0: convert_date}
)


def main():
    patternStorage()
    currentPattern()
    patternRecognition()
    totalTime = time.time() - totalStart
    print('Entire Processing time took: ', totalTime, ' seconds')


def percentChange(startPoint, currentPoint):
    try:
        x = ((float(currentPoint) - startPoint) / abs(startPoint)) * 100.00
        if x == 0:
            return 0.0000000001
        else:
            return x
    except ZeroDivisionError:
        return 0.0000000001

def patternStorage():
    patStartTime = time.time()
    x = len(avgLine) - 60
    y = 31

    while y < x:
        pattern = []
        for index in range(29, -1, -1):
            pattern.append(percentChange(avgLine[y - 30], avgLine[y - index]))

        outcomeRange = avgLine[y + 20: y + 30]
        currentPoint = avgLine[y]
        try:
            avgOutcome = functools.reduce(lambda x, y: x + y, outcomeRange) / len(outcomeRange)
        except Exception as e:
            print(str(e))
            avgOutcome = 0

        futureOutcome = percentChange(currentPoint, avgOutcome)
        patternAr.append(pattern)
        performanceAr.append(futureOutcome)

        y += 1

    patEndTime = time.time()
    print(len(patternAr))
    print(len(performanceAr))
    print('Pattern storage took: ', patEndTime - patStartTime, ' seconds')


def currentPattern():
    for index in range(30):
        patForRec.append(percentChange(avgLine[-31], avgLine[-30 + index]))



def patternRecognition():
    sim = 0
    patFound = 0
    plotPatAr = []
    predictedOutcomesAr = []

    for eachPattern in patternAr:
        for index in range(len(eachPattern)):
            sim += 100.00 - abs(percentChange(eachPattern[index], patForRec[index]))

        howSim = sim / 30.00
        sim = 0

        if howSim > 70:
            patdex = patternAr.index(eachPattern)
            patFound = 1

            xp = range(1, 31)
            plotPatAr.append(eachPattern)

    if patFound == 1:
        fig = plt.figure(figsize=(10, 6))

        for eachPatt in plotPatAr:
            futurePoints = patternAr.index(eachPatt)

            if performanceAr[futurePoints] > patForRec[29]:
                pcolor = '#24bc00'
            else:
                pcolor = '#d40000'

            plt.plot(xp, eachPatt)
            predictedOutcomesAr.append(performanceAr[futurePoints])
            plt.scatter(35, performanceAr[futurePoints], c=pcolor, alpha=.3)

        realOutcomeRange = allData[toWhat+20:toWhat+30]
        realAvgOutcome = functools.reduce(lambda x, y: x + y, realOutcomeRange) / len(realOutcomeRange)
        realMovement = percentChange(allData[toWhat], realAvgOutcome)
        predictedAvgOutcome = functools.reduce(lambda x, y: x + y, predictedOutcomesAr) / len(predictedOutcomesAr)

        plt.scatter(40, realMovement, c='#54fff7', s=25)
        plt.scatter(40, predictedAvgOutcome, c='b', s=25)

        plt.plot(xp, patForRec, '#54fff7', linewidth=3, label='Pattern for recognition')
        plt.grid(True)
        plt.title('Pattern Recognition')
        plt.legend()
        plt.show()


def graphRawFX():
    fig = plt.figure(figsize=(10, 7))
    ax1 = plt.subplot2grid((40, 40), (0, 0), rowspan=40, colspan=40)

    ax1.plot(date, bid)
    ax1.plot(date, ask)
    plt.gca().get_yaxis().get_major_formatter().set_useOffset(False)
    ax1.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M:%s'))

    for label in ax1.xaxis.get_ticklabels():
        label.set_rotation(45)

    ax1_2 = ax1.twinx()

    ax1_2.fill_between(date, 0, (ask - bid), facecolor='g', alpha=.3)
    plt.subplots_adjust(bottom=.23)
    plt.xlabel('DateTime')
    plt.ylabel('Price')
    plt.grid(True)
    plt.show()

dataLength = int(bid.shape[0])
print('Data length is ', dataLength)

toWhat = 37000
allData = ((bid + ask) / 2)

while toWhat < dataLength:
    avgLine = allData[:toWhat]
    patForRec = []
    patternAr = []
    performanceAr = []

    if __name__ == "__main__": main()
    moveOn = input('press ENTER to continue');
    toWhat += 1
